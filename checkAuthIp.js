const axios = require("axios");
const fs = require("fs/promises");
const shell = require("shelljs");
setInterval(async () => {
  const { data } = await axios.get("http://ip-api.com/json");
  if (data?.query) {
    const ip = data?.query;
    const proxyAuth = await fs.readFile("./proxy-rotation-ipauth.sh", "utf-8");
    const macthIp = proxyAuth.replace(
      /IP_AUTHORIZATION=.+/,
      `IP_AUTHORIZATION=${ip}`
    );
    await fs.writeFile("./proxy-rotation-ipauth.sh", macthIp);
    shell.exec("git add .");
    shell.exec("git commit -m ok");
    shell.exec("git push");
  }
}, 60000);
