#!/usr/bin/bash
gen_ipv6_64() {
	#Backup File
	rm $WORKDIR/ipv6.txt
	count_ipv6=1
	while [ "$count_ipv6" -le $MAXCOUNT ]
	do
		array=( 1 2 3 4 5 6 7 8 9 0 a b c d e f )
		ip64() {
			echo "${array[$RANDOM % 16]}${array[$RANDOM % 16]}${array[$RANDOM % 16]}${array[$RANDOM % 16]}"
		}
		echo $IP6:$(ip64):$(ip64):$(ip64):$(ip64) >> $WORKDIR/ipv6.txt
		let "count_ipv6 += 1"
	done
}


gen_3proxy_cfg() {
	echo daemon
	echo maxconn 3000
	echo nserver 1.1.1.1
	echo nserver [2606:4700:4700::1111]
	echo nserver [2606:4700:4700::1001]
	echo nserver [2001:4860:4860::8888]
	echo nscache 65536
	echo timeouts 1 5 30 60 180 1800 15 60
	echo setgid 65535
	echo setuid 65535
	echo stacksize 6291456 
	echo flush
    echo auth none
	
	port=$START_PORT
	while read ip; do
		echo "proxy -6 -n -a -p$port -i$IP4 -e$ip"
		((port+=1))
	done < $WORKDIR/ipv6.txt
	
}


gen_ifconfig() {
	while read line; do    
		echo "ifconfig $IFCFG inet6 add $line/64"
	done < $WORKDIR/ipv6.txt
}


if [ "x$(id -u)" != 'x0' ]; then
    echo 'Error: this script can only be executed by root'
    exit 1
fi

service network restart

echo "Kiểm tra kết nối IPv6 ..."


if ip -6 route get 2606:4700:4700::1111 &> /dev/null
then
	IP4="2.58.200.156"
	IP6="2a05:4140:600:5e"
	main_interface="eth0"
	
    echo "[OKE]: Thành công"
    	echo "IPV4: 2.58.200.156"
	echo "IPV6: 2a05:4140:600:5e"
	echo "Mạng chính: eth0"
else
    echo "[ERROR]:  thất bại!"
	exit 1
fi

IFCFG="eth0" 
WORKDIR="/root"
START_PORT=14000
MAXCOUNT=2000
echo "Đang tạo $MAXCOUNT IPV6 > ipv6.txt"
gen_ipv6_64


echo "Đang tạo IPV6 gen_ifconfig.sh"
gen_ifconfig >$WORKDIR/boot_ifconfig.sh
bash $WORKDIR/boot_ifconfig.sh

echo "3proxy Start"
gen_3proxy_cfg > /etc/3proxy/3proxy.cfg
killall 3proxy
service 3proxy start

echo "Đã Reset IP"
